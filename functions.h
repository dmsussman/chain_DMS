//assorted utility functions

void matvecprod(const vector<dvec> &mat, const dvec &vec, dvec &ans)
{
    assert(mat[0].size() == vec.size());
    ans.resize(mat.size());
    for (int rr =0; rr < mat.size(); ++rr)
    {
        ans[rr]=0.0;
        for (int cc = 0; cc < vec.size(); ++cc)
            ans[rr]+=mat[rr][cc]*vec[cc];
    };
};

void crossProd(const dvec &a, const dvec &b, dvec &ans)
{
    assert(ans.size() == a.size());
    ans[0] = a[1]*b[2]-a[2]*b[1];
    ans[1] = a[2]*b[0]-a[0]*b[2];
    ans[2] = a[0]*b[1]-a[1]*b[0];
};

double random01(base_generator_type& rnd)
{//a uniformly sampled random number between zero and one
    boost::uniform_real<> uni_dist(0,1);
    boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni(rnd, uni_dist);
    return uni();
};

void spherepoints(base_generator_type& rnd, vector<double> &vec)
{//returns a random point on the surface of the unit sphere in the vector vec
    assert(vec.size() == 3);
    double u = 2.0*M_PI*random01(rnd);
    double v = (random01(rnd)-0.5)*2.0;

    double part = sqrt(1-v*v);
    vec[0]=cos(u)*part;
    vec[1]=sin(u)*part;
    vec[2]=v;
}

int randint(base_generator_type& rnd, int min, int max)
{//returns a random integer between min and max
    boost::random::uniform_int_distribution<> range(min,max);
    return range(rnd);
};
