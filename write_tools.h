//This header contains all functions to output to files acceptable as input to different common MD packages



void WriteHoomd(vector<dvec> &chainStats, const vector<dvec> &atoms,const dvec &boxls,const dvec &box2, const vector< vector<int> > &iflags,const vector<int> &pbcs)
{
//given a set of particle positions (in the simulation box), the box definition,
//and the image flags
//This function writes a hoomd-style xml file suitable for init methods
//pbcs are passed to know whether to expand the box in the vacuum directions
    int anum = atoms.size();
    char fwritename[256];
    sprintf(fwritename,"./poly_N%i.xml",(int)atoms.size());

    ofstream f(fwritename);
    int timestep = 0;
    f.precision(13);
    //PREAMBLE
    f << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << "\n";
    f << "<hoomd_xml version=\"1.5\">" << "\n";
    f << "<configuration time_step=\"" << timestep << "\" "
        << "dimensions=\"" << DIM << "\" "
        << "natoms=\"" << atoms.size() << "\" "
        << ">" << "\n";
    f << "<box Lx=\""<<box2[0] << "\" Ly=\""<<box2[1] << "\" Lz=\""<< box2[2] << "\" />" << "\n";

    //POSITIONS
    dvec halfbox(DIM);
    for (int dd = 0; dd < DIM; ++dd)
        halfbox[dd] = 0.5* boxls[dd];
    f << "<position>" <<"\n";
    for (int aa = 0; aa < anum; ++aa)
        f << atoms[aa][0]-halfbox[0] << " " << atoms[aa][1]-halfbox[1] << " " << atoms[aa][2]-halfbox[2] << "\n";
    f << "</position>" <<"\n";

    //IMAGE FLAGS
    f << "<image>" <<"\n";
    for (int aa = 0; aa < anum; ++aa)
        f << iflags[aa][0] << " " << iflags[aa][1] << " " << iflags[aa][2] << "\n";
    f << "</image>" <<"\n";



    //TYPES
    f << "<type>" <<"\n";
    for (int cs = 0; cs < chainStats.size(); ++cs)
    {
        dvec tc = chainStats[cs];
        int nMon = tc[0];
        int nChain = tc[1];
        int aType = tc[12];
        for (int aa = 0; aa < nMon*nChain; ++aa)
            f << "A"<<aType << "\n";

    };
    f << "</type>" <<"\n";

    //BONDS
    f << "<bond>" <<"\n";
    int pi = 0;
    for (int cs = 0; cs < chainStats.size(); ++cs)
    {
        dvec tc = chainStats[cs];
        int nMon = tc[0];
        int nChain = tc[1];
        int bType = tc[13];
        for (int cc = 0; cc < nChain; ++cc)
        {
            for (int mm = 0; mm < nMon-1; ++mm)
            {
                f <<"polymer"<<bType<<" " << pi << " " <<pi+1 <<"\n";
                pi+=1;
            };
            pi+=1;
        };
    };
    f << "</bond>" <<"\n";


    //ANGLES
    f << "<angle>" << "\n";
    int startAtom = 0;
    for (int cs = 0; cs < chainStats.size(); ++cs)
    {
        dvec tc = chainStats[cs];
        int nMon=tc[0];
        int nChain=tc[1];
        int aType = tc[12];
        bool checkAngle = false;
        if (tc[5]>0.5) checkAngle = true;
        if(!checkAngle) startAtom +=nMon*nChain;
        if (checkAngle)
        {
            for (int cc = 0; cc < nChain; ++cc)
            {
                for (int mm = 0; mm < nMon-2; ++mm)
                {
                    f <<"A"<<aType<<"-A"<<aType<<"-A"<<aType<<" " << startAtom << " " << startAtom+1 << " " <<startAtom+2 <<"\n";
                    startAtom +=1;
                };
                startAtom+=2;
            };
        };
    };
    f << "</angle>" << "\n";

    //DIHEDRAL
    f << "<dihedral>" << "\n";
    startAtom = 0;
    for (int cs = 0; cs < chainStats.size(); ++cs)
    {
        dvec tc = chainStats[cs];
        int nMon=tc[0];
        int nChain=tc[1];
        int aType = tc[12];
        bool checkDihedral = false;
        if (tc[8]>0.5) checkDihedral = true;
        if(!checkDihedral) startAtom +=nMon*nChain;
        if (checkDihedral)
        {
            for (int cc = 0; cc < nChain; ++cc)
            {
                for (int mm = 0; mm < nMon-3; ++mm)
                {
                    f <<"A"<<aType<<"-A"<<aType<<"-A"<<aType<<"-A"<<aType<<" " << startAtom << " " << startAtom+1 << " " <<startAtom+2 << " " <<startAtom+3 << "\n";
                    startAtom +=1;
                };
                startAtom+=3;
            };
        };
    };
    f << "</dihedral>" << "\n";


    //CLOSING LINES OF FILE
    f << "</configuration>" << "\n";
    f << "</hoomd_xml>" << "\n";
    f.close();
};

