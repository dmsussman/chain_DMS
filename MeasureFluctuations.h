///This header file defines a few different methods for computing local density fluctuations


void makeSimpleCells(const dvec &boxLs, double scale,vector<int> &cells, dvec &binsizeout, ivec &binsout)
{
    //set up a simple partitioning of space into cubic grid points
    dvec binsize(DIM);
    vector<int> bins(DIM);
    int binnum=1;
    for (int dd = 0; dd < DIM; ++dd)
    {
        bins[dd] = (int)(floor(boxLs[dd]/scale));
        binnum = binnum*bins[dd];
        binsize[dd] = boxLs[dd]/(float)bins[dd];
        bins[dd] = (int)(ceil(boxLs[dd]/binsize[dd]));
    };
    binsizeout =binsize;
    binsout = bins;
    vector<int> cell2(binnum,0);
    cells=cell2;
};

void changeCells(vector<int> &cells, const vector<dvec> &atoms, const dvec &binsize, const ivec &bins, int change)
{
    //adjust the occupancy of cells (either positive or negative) according to the positions in list "atoms"
    for (int nn = 0; nn < atoms.size(); ++nn)
    {
        int cx = floor((atoms[nn][0])/binsize[0]);
        int cy = floor((atoms[nn][1])/binsize[1]);
        int cz = floor((atoms[nn][2])/binsize[2]);
        int index = cx+cy*bins[0]+cz*bins[0]*bins[1];
        cells[index]+=change;
    };
};

double measureCellDensityFluctuations(const vector<int> &cells, double &mini, double &mean, double &max, dvec &low, const ivec &bins, const dvec &binsize)
{
    //takes a cell list (cubic partitioning of space) and checks fluctuations in cell occupancy
    //... a faster way to reduce density fluctuations with MC moves since it is much easier to compute
    mean = 0.0;
    mini = 1000.0;
    max = 0.0;
    float mean2= 0.0;
    int binnum = cells.size();
    int cellidx;
    for (int bb = 0; bb < binnum; ++bb)
    {
        mean += cells[bb];
        mean2 += cells[bb]*cells[bb];
        if (cells[bb] > max)
            max = (float)cells[bb];
        if (cells[bb] < mini)
        {
            mini = (float)cells[bb];
            cellidx=bb;
        };
    };

    int cc[DIM] = {0,0,0};
    cc[0] = cellidx % bins[0];
    cc[1] = cellidx - cc[0];
    while (cc[1] > bins[0]*bins[1])  cc[1] -= bins[0]*bins[1];
    cc[1] /= bins[0];
    cc[2] = (cellidx - cc[0] - cc[1]*bins[0])/(bins[0]*bins[1]);
    for (int dd = 0; dd < DIM; ++dd)
        low[dd] = ((float)cc[dd]+0.5)*binsize[dd];


    mean /=binnum;
    mean2/=binnum;
    return (mean2-mean*mean);
}


double measureDensityFluctuationsLocal(const vector< dvec> &Atoms, const dvec &boxLs, double &scale, double &mini, double &mean, dvec &low)
{
    //measures fluctuations in neighbor number out to a distance scale...
    SimpleGrid grid(Atoms,boxLs,scale);

    vector<vector<SimpleNeighbor> > Neighbors;
    list<int> getNeighList;
    for (int aa = 0; aa < Atoms.size(); ++aa)
    {
        for (int dd = 0; dd < DIM; ++dd)
        {
            assert(Atoms[aa][dd] > 0);
            assert(Atoms[aa][dd] < boxLs[dd]);
        };
        getNeighList.push_back(aa);
    };
    grid.PopulateNeighborList(Atoms,scale*scale,getNeighList, Neighbors);

    mean = 0.;
    float mn2 = 0.;
    int smallest=100000;
    scale=0.;
    int lowint = 0;
    for (int aa=  0; aa < Atoms.size(); ++aa)
    {
        int size = Neighbors[aa].size();
        if(size > scale)
            scale = size;
        if(size < smallest)
        {
            smallest = size;
            lowint = aa;
        };
        mean += (float) size;
        mn2 += (float) size*size;
    };
    mini = smallest;
    mean = mean/Atoms.size();
    mn2 = mn2/Atoms.size();
    for (int dd = 0; dd < DIM; ++dd)
        low[dd] = Atoms[lowint][dd]+0.5;

    return (mn2-mean*mean);
};

