#include <boost/random.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace boost;

#define DIM 3

typedef boost::mt19937 base_generator_type;
typedef std::vector<double> dvec;
typedef std::vector<int> ivec;

#include "SimpleGrid.h"
#include "simple_box.h"
#include "functions.h"
#include "write_tools.h"
#include "MC_moves.h"
#include "MeasureFluctuations.h"


int makeChain(base_generator_type& rnd, int nMonomer, double bondLength, double bondMin, int swapType, const dvec &boxLs, const vector<int> &pbcs, vector< dvec> &chain, vector< dvec > &atoms, vector<int> &moltype, bool checkAngle, double cosT0, double Tk, bool checkDihedral, double phi0, double phik, double phin, double McTemp, dvec &chainStart)
{
//given a desired number of monomers, bondlength, minimum distance,
//as well as a box and its boundary conditions, this returns a non-reversing chain (can be outside the box)
//and atoms (coordinates in the box) and the molecular type labels (for swapping)
// If chainStart has positive entries, it gives a starting point for the chain

//Angle    potential: Tk*(cosTijk - cosT0)^2
//
//Dihedral potential: phik*(1+cos(n_{ijkl}\phi_{ijkl}-phi0)),
//                      n_{ijkl} is multiplicity (number of minima as the bond is rotated through 2pi)
//                      phi_{ijkl} = -atan2(sin phi_ijkl/cos phi_ijkl), phi_ijkl is the torsion angle between the four monomers

    double bondMin2=bondMin*bondMin;
    moltype.resize(nMonomer);
    vector< vector<double> > newchain(nMonomer, vector<double>(DIM,0.0));
    chain = newchain;
    atoms = newchain;

    //initialize array and pick a random starting point for the chain
    dvec x0(DIM);
    dvec x1(DIM);
    dvec x2(DIM);
    for (int dd = 0; dd < DIM; ++dd)
    {
        x1[dd] = 0.5*boxLs[dd];
        x2[dd] = boxLs[dd]*random01(rnd);
//        if (chainStart[dd] >=0) x2[dd] = chainStart[dd];
    };
    x0=x1;

    chain[0]=x2;
    atoms[0]=x2;
    moltype[0]=1;

    int mcmoves = 0;
    for (int nn = 1; nn < nMonomer; ++nn)
    {
        x0=x1;
        x1=x2;

        //get a non-reversing random point on the unit sphere
        bool goodBond = false;
        while (goodBond == false)
        {
            mcmoves +=1;
            if (mcmoves > 10000*nMonomer){
                cout << "Trying too hard to make this chain.... Trying again" << endl;
                return 0;
                break;
            };
            //first, get a new potential point on the chain by picking randomly
            dvec bond(DIM);
            dvec RR(DIM);
            spherepoints(rnd,bond);
            double norm = 0.0;
            for (int dd = 0; dd < DIM; ++dd)
            {
                x2[dd]=x1[dd]+bondLength*bond[dd];
                //if the box has reflecting walls, make sure the new proposed site is in the box.
                if (pbcs[dd] != 0)
                    x2[dd] = multipleReflections(x2[dd],boxLs[dd]);
                RR[dd]=x2[dd]-x0[dd]; // will be used to check if the rw step is reversing or not
                norm += RR[dd]*RR[dd];
            };
            //Now, prepare info about the angle between this step and the last one... if nn = 1 this will be ignored later

            dvec rjk(DIM);
            dvec rlk(DIM);
            double cosTijk = 0.0;
            for(int dd = 0; dd < DIM; ++dd)
            {
                rjk[dd]=(x0[dd]-x1[dd])/bondLength;
                rlk[dd]=(x2[dd]-x1[dd])/bondLength;
                cosTijk += rjk[dd]*rlk[dd];
            };
            bool goodAngle = true;
            double cost = 0.0;
            double dE = 0.0;

            //if an angle potential is used, add the angle cost to the energy
            if (checkAngle)
            {
                goodAngle = false;
                dE += Tk*(cosTijk-cosT0)*(cosTijk-cosT0);
            };

            //if a dihedral potential is being used, add that cost to the energy, too
            double phiijkl=0.0;
            if(checkDihedral && nn  > 2)
            {
                goodAngle = false;
                dvec nvec(DIM);
                crossProd(rlk,rjk,nvec);
                dvec xn1(DIM);
                xn1 = chain[nn-3];
                dvec rij(DIM);
                for (int dd = 0; dd < DIM; ++dd)
                    rij[dd] = (xn1[dd]-x0[dd])/bondLength;
                dvec mvec(DIM);
                crossProd(rij,rjk,mvec);
                double cosPijkl = 0.0;
                double sinPijkl = 0.0;
                for (int dd = 0; dd < DIM; ++dd)
                {
                    cosPijkl += mvec[dd]*nvec[dd];
                    sinPijkl += nvec[dd]*rij[dd];
                };
                phiijkl = -atan2(sinPijkl,cosPijkl);
                dE+= phik*(1.0+cos(phin*phiijkl-phi0));
            }

            //check for acceptance of bond based on:
            //  (1) boltzmann acceptance on angle and dihedral potentials
            //  (2) non-reversing even if there is no angle cost
            cost = exp(-dE/McTemp);
            double p = random01(rnd);
            if(p < cost) goodAngle = true;

            if (norm > bondMin2&& goodAngle)
            {
                goodBond = true;
            };
            if (nn ==1)
                goodBond = true;
        };

        dvec shiftpoint(DIM);
        shiftpoint = x2;

        dvec inbox(DIM);
        keepInBoxNonperiodic(shiftpoint,boxLs,pbcs, inbox);
        for (int dd = 0; dd < DIM; ++dd)
        {
            if (pbcs[dd] !=0)
                shiftpoint[dd] = multipleReflections(shiftpoint[dd],boxLs[dd]);
        };
        x2=shiftpoint;

        chain[nn] = x2;
        atoms[nn] = inbox;

        int mol = 1;
        if (swapType ==1) mol = nn+1;
        if (swapType ==2)
            if (nn+1 <= 0.5*(float)(nMonomer))
                mol = nn+1;
            if (nn+1 > 0.5*(float)(nMonomer))
                mol = nMonomer-nn;
        moltype[nn] = mol;
    };
    //if chainStart is positive, put the cm of the chain at chainStart
    if (chainStart[0] >=0)
    {
        dvec cm(DIM,0.0);
        for (int nn=0; nn < chain.size(); ++nn)
            for (int dd= 0; dd< DIM; ++dd)
                cm[dd]+=chain[nn][dd];
        for (int dd = 0; dd < DIM; ++dd)
            cm[dd] =cm[dd] / (float)(chain.size());
        for (int nn = 0; nn < chain.size(); ++nn)
            for (int dd = 0; dd < DIM; ++dd)
                chain[nn][dd] = chain[nn][dd]-cm[dd]+chainStart[dd];
        ChainStartsInBox(chain,boxLs,pbcs);
    };

    return 1;
};


void chainSys(base_generator_type& rnd, vector<dvec > &chainStats, double rhostar, int mcperchain, dvec &aspectRatio, vector<int> pbcs,double MCTemp)
{
    vector<int> mpcs, cpcs;
    dvec tc(14);
    int nMon, nChain, swapType,atomType,bondType;
    bool checkAngle, checkDihedral;
    double bondLength, bondMin, cosT0, thetaK,phi0,phik,phin;
    int totalAtoms = 0;
    int totalChains = 0;
    int longestChain = 0;
    bool npbcsFlag = false; //are there any reflecting walls?
    if(pbcs[0] != 0  || pbcs[1] != 0 || pbcs[2] != 0) npbcsFlag = true;
    bool MCRestrict = false; //angle or dihedrals combined with reflecting walls mean that most MC
                             //moves should be prohibited
    cout <<endl;
    for (int cs = 0; cs < chainStats.size(); ++cs)
    {
            tc = chainStats[cs];
            nMon = (int) tc[0]; nChain = (int) tc[1];
            swapType = (int)tc[4]; atomType = (int)tc[12]; bondType = (int)tc[13];
            bondLength = tc[2]; bondMin = tc[3]; cosT0 = tc[6]; thetaK = tc[7];
            phi0 = tc[9]; phik = tc[10]; phin = tc[11];
            checkAngle = false; if(tc[5] > 0.5) checkAngle = true;
            checkDihedral = false; if(tc[8] > 0.5) checkDihedral = true;
            if(npbcsFlag &&(checkAngle ||checkDihedral))
                MCRestrict = true;
            mpcs.push_back(nMon);
            cpcs.push_back(nChain);
            totalAtoms += nChain*nMon;
            totalChains += nChain;
            if (nMon > longestChain) longestChain = nMon;
    };
    cout <<"Making a system with "<< totalAtoms <<" atoms across " <<totalChains << endl;
    for (int cs = 0; cs < chainStats.size(); ++cs)
        cout << mpcs[cs] <<" Monomers per " << cpcs[cs] << " chains" << endl;
    cout <<"Dimensionless density = " << rhostar << endl;
    cout <<endl << "Boundary conditions are: \t  "<<pbcs[0] <<" \t "<<pbcs[1]<<" \t "<<pbcs[2]<<endl;
    int nAtoms = totalAtoms;
    double volume = (float)nAtoms/rhostar;
    double cuberoot = pow(volume,1.0/3.0);

    double arprod=1.0;
    for (int dd = 0; dd < DIM; ++dd)
        arprod *=aspectRatio[dd];
    arprod =pow(arprod,1.0/3.0);
    for (int dd = 0; dd < DIM; ++dd)
        aspectRatio[dd] /=arprod;
    dvec boxls(DIM);
    for (int dd = 0; dd < DIM; ++dd)
        boxls[dd] = cuberoot*aspectRatio[dd];
    cout << "Box dimensions: "<<boxls[0] << "\t" << boxls[1]<< "\t" << boxls[2] << endl << endl;

    vector< dvec> c1,a1;
    vector<int> mt;


    vector< vector<double> > Atoms(nAtoms,vector<double>(DIM,0.0));
    vector< vector<int> > imageFlags(nAtoms,vector<int>(DIM,0));
    vector<int> moltype(nAtoms);
    vector< vector<double > > basechain(longestChain,vector<double>(DIM,0.0));
    vector< vector< vector<double> > > Chains(totalChains,basechain);

    dvec xmin(DIM,10*boxls[0]);
    dvec xmax(DIM,-10000);
    dvec diff(DIM);
    int ind = 0;
    int chainCount = 0;
    for (int cs = 0; cs < chainStats.size(); ++cs)
    {
        tc = chainStats[cs];
        nMon = (int) tc[0]; nChain = (int) tc[1];
        swapType = (int)tc[4]; atomType = (int)tc[12]; bondType = (int)tc[13];
        bondLength = tc[2]; bondMin = tc[3]; cosT0 = tc[6]; thetaK = tc[7];
        phi0 = tc[9]; phik = tc[10]; phin = tc[11];
        bool checkA = false; if(tc[5] > 0.5) checkA = true;
        bool checkD = false; if(tc[8] > 0.5) checkD = true;

        double Ree = 0;

        for (int cc = 0; cc < nChain; ++cc)
        {
            int check = 0;
            dvec chainStart(DIM,-10.0);
            while(check ==0)
                check=makeChain(rnd,nMon,bondLength,bondMin,swapType,boxls,pbcs,c1,a1,mt,checkA,cosT0,thetaK,checkD,phi0,phik,phin,MCTemp,chainStart);
            Chains[chainCount] = c1;
            chainCount +=1;
            diff[0] = c1[0][0]-c1[nMon-1][1];
            diff[1] = c1[0][1]-c1[nMon-1][1];
            diff[2] = c1[0][2]-c1[nMon-1][2];
            Ree+=  pow(diff[0]*diff[0]+diff[1]*diff[1]+diff[2]*diff[2],0.5);
            for (int mm = 0; mm < nMon; ++mm)
            {
                Atoms[ind]=a1[mm];
                moltype[ind]=mt[mm];
                dvec xpos(DIM);
                xpos  = Chains[cc][mm];
                for (int dd = 0; dd < DIM; ++dd)
                {
                    if (xpos[dd] < xmin[dd]) xmin[dd] = xpos[dd];
                    if (xpos[dd] > xmax[dd]) xmax[dd] = xpos[dd];

                }
                ind +=1;
            };

        };
        cout <<"set " << cs << " <Ree> = " << Ree/(float)nChain << endl;
    };


    cout <<" Minimum and maximum monomer position (ignoring BCs)" << endl;
    for (int dd = 0; dd < DIM; ++dd)
        cout << xmin[dd] << "    " << xmax[dd] << endl;
   // cout << "Chain stats: ";
   // for (int cc = 0; cc < Chains.size(); ++cc)
   //     cout << Chains[cc].size() << "   ";
   // cout << endl;
    double minidist,meanneigh;
    minidist = 1.0;
    meanneigh = 1.0;
    dvec low(DIM);
    double fscale = 4.5;
    double scale = fscale;
    double rhofluct = measureDensityFluctuationsLocal(Atoms, boxls, scale,minidist,meanneigh,low);
    double initdrho = rhofluct;
    cout << endl <<"initial <drho>=\t" << rhofluct <<  "\t  low density: "<<minidist << "\t high density: " << scale << endl;
    cout << "initial lowest density spot = " << low[0] <<"   " <<low[1]<< "  " << low[2] << "; local density ~ " << minidist << " vs. mean density ~ " << meanneigh << endl;


    scale=fscale;
    vector<int> cells;
    dvec binsize;
    ivec bins;
    makeSimpleCells(boxls,scale,cells,binsize,bins);
    changeCells(cells,Atoms,binsize,bins,1);
    rhofluct = measureCellDensityFluctuations(cells,minidist,meanneigh,scale,low,bins,binsize);
    int lowfluct = minidist;
    scale = fscale;
    double initfluct = rhofluct;

    //Perform Monte Carlo chain moves to try to lower density fluctuations
    //This changes the chains without altering the image flags
    cout << endl << "Now attempting an average of " <<mcperchain <<" MC moves per chain" << endl;
    if (MCRestrict)
            cout << endl << "MC moves restricted to chain replacement!" << endl << endl;
    vector< vector<double> > Atoms1(nAtoms,vector<double>(DIM,0.0));
    vector<double> attempt(10,0.0);
    vector<double> accept(10,0.0);
    vector< dvec> tc1,tc2,tc3,tc4,tc5,cc1,cc2;
    int chainindex1,chainindex2,chainindex3,chainindex4;
    int chosenChain, chosenChain2,chainisfromset,chaincount,baseIndex,baseChain;
    for (int aa = 0; aa < mcperchain*Chains.size(); ++aa)
    {
        chosenChain = randint(rnd,0,Chains.size()-1);
        chainisfromset = 0;
        chaincount = cpcs[0];
        baseIndex = 0;
        baseChain = 0;
        while(chosenChain >= chaincount)
        {
            baseChain +=cpcs[chainisfromset];
            baseIndex += mpcs[chainisfromset]*cpcs[chainisfromset];
            chainisfromset += 1;
            chaincount += cpcs[chainisfromset];
        };
        chainindex1 = (chosenChain-baseChain)*mpcs[chainisfromset]+baseIndex;
        chainindex2 = chainindex1 + mpcs[chainisfromset];
        tc1=Chains[chosenChain];
        tc2=tc1;
        int maxmc = 5;
        int mcmove = randint(rnd,0,maxmc);
        if (MCRestrict) mcmove = 9;
        if (mcmove > maxmc) mcmove = 3;
        attempt[mcmove] +=1.0;
        dvec rotvec(DIM);
        dvec refvec(DIM);
        switch(mcmove){
            case 0:  {double transMag = 0.1;
                     ChainTrans(rnd,tc1,boxls,tc2,transMag);
                     ChainStartsInBox(tc2,boxls,pbcs);
                     break;}
            case 1:  {ChainInvert(tc2);
                     ChainStartsInBox(tc2,boxls,pbcs);
                     break;}
            case 2:  {spherepoints(rnd,refvec);
                     ChainReflect(tc2,refvec);
                     ChainStartsInBox(tc2,boxls,pbcs);
                     break;}
            case 3:  {spherepoints(rnd,rotvec);
                     double rotmag=random01(rnd)*2.0*M_PI*0.05;
                     ChainRotate(tc2,rotvec,rotmag);
                     ChainStartsInBox(tc2,boxls,pbcs);
                     break;}
            case 4: {
                        chosenChain2 = randint(rnd,baseChain,baseChain+cpcs[chainisfromset]-1);
                        chainindex3  = (chosenChain2-baseChain)*mpcs[chainisfromset]+baseIndex;
                        chainindex4  = chainindex3+mpcs[chainisfromset];
                        tc4 = Chains[chosenChain2];
                        ChainSwap(tc2,tc4);
                        ChainStartsInBox(tc2,boxls,pbcs);
                        ChainStartsInBox(tc4,boxls,pbcs);
                        tc5=tc4;
                        putChainInBox(tc5,boxls,pbcs);
                     };
            default:  {dvec pp = chainStats[chainisfromset];
                     nMon = (int) pp[0]; swapType = (int)pp[4]; bondLength = pp[2];
                     bondMin = pp[3]; cosT0=pp[6]; thetaK=pp[7];
                     phi0 = pp[9]; phik = pp[10]; phin = pp[11];
                     checkAngle = false; if(pp[5] > 0.5) checkAngle = true;
                     checkDihedral = false; if(pp[8] > 0.5) checkDihedral = true;
                     int check = 0;
                     dvec chainStart(DIM,-10.0);
                     chainStart=low;
                     while(check ==0)
                         check = makeChain(rnd,nMon,bondLength,bondMin,swapType,boxls,pbcs,tc2,a1,mt,checkAngle,cosT0,thetaK,checkDihedral,phi0,phik,phin,MCTemp,chainStart);
                     break;}
        };
        tc3=tc2;
        putChainInBox(tc3,boxls,pbcs);
        cc1.resize(tc1.size());
        for (int mm = 0; mm < cc1.size(); ++mm) cc1[mm] = Atoms[mm+chainindex1];
        changeCells(cells,cc1,binsize,bins,-1);
        changeCells(cells,tc3,binsize,bins,1);
        if(mcmove==4)
        {
            cc2.resize(tc4.size());
            for (int mm = 0; mm < cc2.size(); ++mm) cc2[mm] = Atoms[mm+chainindex3];
            changeCells(cells,cc2,binsize,bins,-1);
            changeCells(cells,tc5,binsize,bins,1);
        };


        double scale2 = fscale;
        double rhofluct2 = measureCellDensityFluctuations(cells,minidist,meanneigh,scale2,low,bins,binsize);
        int lowfluct2 = minidist;
        if (rhofluct2 < rhofluct)
        //if (lowfluct2 < lowfluct)
        {
            accept[mcmove] += 1.0;
            Chains[chosenChain]=tc2;
            rhofluct=rhofluct2;
            lowfluct=lowfluct2;
            for (int mm = 0; mm < tc3.size(); ++mm)
                Atoms[mm+chainindex1] = tc3[mm];
            if(mcmove==4)
            {
                Chains[chosenChain2]=tc4;
                for (int mm = 0; mm < tc5.size(); ++mm)
                    Atoms[mm+chainindex3] = tc5[mm];
                cells.clear();
                makeSimpleCells(boxls,scale,cells,binsize,bins);
                changeCells(cells,Atoms,binsize,bins,1);
            };
        }else{
            changeCells(cells,cc1,binsize,bins,1);
            changeCells(cells,tc3,binsize,bins,-1);
            if(mcmove==4)
            {
                changeCells(cells,cc2,binsize,bins,1);
                changeCells(cells,tc5,binsize,bins,-1);
            };
        };

        if (aa % (int)(totalChains*mcperchain/10) ==0)
        {
            cout << "progress=" << (float)(aa)/(float)(totalChains*mcperchain) <<  "\t, fluctuations=" << rhofluct << "\t ,percent of initial= " << rhofluct/initfluct*100.0 << "\t  low density: "<<minidist << "\t high density: " << scale2 << endl;
        };
    };
    if(mcperchain>0)
    {
        cout << setprecision(4);
        cout << endl << "MC stats:\t\tPercent of mcmoves\t\tAcceptance rate"<<endl;
        cout << "translate\t\t" << attempt[0]/(totalChains*mcperchain) <<"\t\t\t\t"<<accept[0]/attempt[0] << endl;
        cout << "inversion\t\t" << attempt[1]/(totalChains*mcperchain) <<"\t\t\t\t"<<accept[1]/attempt[1] << endl;
        cout << "reflection\t\t" << attempt[2]/(totalChains*mcperchain) <<"\t\t\t\t"<<accept[2]/attempt[2] << endl;
        cout << "rotation \t\t" << attempt[3]/(totalChains*mcperchain) <<"\t\t\t\t"<<accept[3]/attempt[3] << endl;
        cout << "exchange \t\t" << attempt[4]/(totalChains*mcperchain) <<"\t\t\t\t"<<accept[4]/attempt[4] << endl;
        float regrowAt = 0.0;
        float regrowAc = 0.0;
        for (int mcmc = 5; mcmc < 10; ++mcmc)
        {
            regrowAt += attempt[mcmc];
            regrowAc += accept[mcmc];
        };
        cout << "replacement\t\t" << regrowAt/(totalChains*mcperchain) <<"\t\t\t\t"<<regrowAc/regrowAt << endl<<endl;
    };
    //MC moves section finished
    //
    //
    scale = fscale;
    rhofluct = measureDensityFluctuationsLocal(Atoms, boxls, scale,minidist,meanneigh,low);

    cout << "Final <drho>=\t" << rhofluct <<  "\t percent: " << rhofluct/initdrho << "\t  low density: "<<minidist << "\t high density: " << scale << endl;
    cout << "final lowest density spot = " << low[0] <<"   " <<low[1]<< "  " << low[2]  << endl;


    cout <<endl;
    dvec xmin2(DIM,10*boxls[0]);
    dvec xmax2(DIM,-10000);
    cout << endl << "Setting image flags and preparing to export" << endl;
    dvec box2 = boxls;
    for (int dd = 0; dd < DIM; ++dd)
    {
        if(pbcs[dd] != 0)
            box2[dd] = round(boxls[dd])+40.0;
    };
    int idx = 0;
    int large_image = 0;
    for (int cc = 0; cc < totalChains; ++cc)
    {
        double chainDist = 0.0;
        int nmon2 = Chains[cc].size();
        for (int mm = 0; mm < nmon2; ++mm)
        {
            dvec xpos(DIM);
            dvec apos(DIM);
            xpos  = Chains[cc][mm];
            apos = Atoms[idx];
            for (int dd = 0; dd < DIM; ++dd)
            {
                assert(apos[dd]>=0.0);
                assert(apos[dd]<=boxls[dd]);
                double dist = round((xpos[dd]-apos[dd])/boxls[dd]);
                imageFlags[idx][dd] = (int)(dist);
                if((int)fabs(dist)>large_image) large_image = (int)fabs(dist);
                if (xpos[dd] < xmin2[dd]) xmin2[dd] = xpos[dd];
                if (xpos[dd] > xmax2[dd]) xmax2[dd] = xpos[dd];
            };
            if(mm < nmon2-1)
            {
                dvec apos2 = Atoms[idx+1];
                double distance = mindistsquared(apos,apos2,box2);
                if (distance > chainDist) chainDist = distance;
            };
            idx +=1;
        };
    };
    cout <<" Minimum and maximum monomer position (ignoring BCs)" << endl;
    for (int dd = 0; dd < DIM; ++dd)
        cout << xmin2[dd] << "    " << xmax2[dd] << endl;
    cout << "largest image flag = " << large_image << endl;

    WriteHoomd(chainStats,Atoms,boxls,box2,imageFlags,pbcs);
};



int main(int argc, char*argv[])
{
    int c;

    while((c=getopt(argc,argv,"n:m:s:r:b:x:y:z:p:")) != -1)
        switch(c)
        {
            case '?':
                    if(optopt=='c')
                        std::cerr<<"Option -" << optopt << "requires an argument.\n";
                    else if(isprint(optopt))
                        std::cerr<<"Unknown option '-" << optopt << "'.\n";
                    else
                        std::cerr << "Unknown option character.\n";
                    return 1;
            default:
                       abort();
        };
//a bunch of default parameters
//overwrite these by having an input.txt file in the same directory as the executable
    int nChainSets = 1;
    int seed = 1;
    double nMon = 100;
    double nChain=100;
    double rho=0.86;
    int mcmovesperchain=100;
    dvec aspectRatio(DIM,1.0);
    vector<int> pbcs(DIM,0);

    double bondLength = 1.0;
    double bondMin = 1.02;
    double swapType = 2;

    double checkAngle = false;
    double cosT0 = -0.7;
    double thetaK = 50.0;

    double checkDihedral = false;
    double phi0 = 0.1;
    double phik = 1.0;
    double phin = 4.0;
    double atomType=0;
    double bondType=0;
    double MCTemp = 1.0;
    vector<dvec > chainStats;
    ifstream inf("input.txt");
    string line,name;
    double var1;
    int idx = 0;

    int ncs = 0;
    while (getline(inf,line))
    {
        istringstream ss(line);
        ss >> name >> var1;
        switch(idx)
        {
            case 0 : cout << "reading file for input parameters" << endl;
                     nChainSets = (int) var1; chainStats.resize(nChainSets);
                     break;
            case 1 : rho = var1; break;
            case 2 : MCTemp = var1; break;
            case 3 : mcmovesperchain = (int) var1; break;
            case 4 : pbcs[0] = (int) var1; break;
            case 5 : pbcs[1] = (int) var1; break;
            case 6 : pbcs[2] = (int) var1; break;
            case 7 : aspectRatio[0] = var1; break;
            case 8 : aspectRatio[1] = var1; break;
            case 9 : aspectRatio[2] = var1; break;
            case 10 : seed = (int) var1; getline(inf,line); break;
        };

        if (idx > 10)
        {
            if (ncs >= chainStats.size()) break;
            dvec tc(14);
            tc[0] = nMon = var1;
            getline(inf,line);istringstream s1(line);s1 >> name >> var1;
            tc[1] = nChain =  var1;
            getline(inf,line);istringstream s2(line);s2 >> name >> var1;
            tc[2] = bondLength = var1;
            getline(inf,line);istringstream s3(line);s3 >> name >> var1;
            tc[3] = bondMin = var1;
            getline(inf,line);istringstream s4(line);s4 >> name >> var1;
            tc[4] = swapType =  var1;
            getline(inf,line);istringstream s5(line); s5 >> name >> var1;
            tc[5] = checkAngle =  var1;
            getline(inf,line);istringstream s6(line);s6 >> name >> var1;
            tc[6] = cosT0 =  var1;
            getline(inf,line);istringstream s7(line);s7 >> name >> var1;
            tc[7] = thetaK =  var1;
            getline(inf,line);istringstream s8(line);s8 >> name >> var1;
            tc[8] = checkDihedral = var1;
            getline(inf,line);istringstream s9(line);s9 >> name >> var1;
            tc[9] = phi0 =  var1;
            getline(inf,line);istringstream s10(line);s10 >> name >> var1;
            tc[10] = phik =  var1;
            getline(inf,line);istringstream s11(line);s11 >> name >> var1;
            tc[11] = phin = var1;
            getline(inf,line);istringstream s12(line);s12 >> name >> var1;
            tc[12] = atomType = var1;
            getline(inf,line);istringstream s13(line);s13 >> name >> var1;
            tc[13] = bondType = var1;
            chainStats[ncs]=tc;
            ncs +=1;
            getline(inf,line);
        };
        idx += 1;

    };
    //IF no file is present, use defaults
    if(chainStats.size() == 0)
    {
        chainStats.resize(1);dvec tc(14);
        tc[0] = nMon; tc[1] = nChain; tc[2] = bondLength;tc[3] = bondMin; tc[4] = swapType;
        tc[5] = checkAngle;tc[6] = cosT0;tc[7] = thetaK;tc[8] = checkDihedral;tc[9] = phi0;tc[10] = phik;
        tc[11] = phin;tc[12] = atomType;tc[13] = bondType;chainStats[0]=tc;
    };


    boost::mt19937 rnd(seed);
    chainSys(rnd,chainStats,rho,mcmovesperchain,aspectRatio,pbcs,MCTemp);

    return 0;
}
